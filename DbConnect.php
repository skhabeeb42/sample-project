<?php
/**
 * Handling database connection
 * Project-Healthicon
 * @author Habeebullah Saheb Shaik
 */
class DbConnect {

    private $conn;

    function __construct() {        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() 
    {
		$serverHost = strtolower($_SERVER['HTTP_HOST']);
		if($serverHost == 'localhost') //local
		{
			return mysqli_connect("localhost","root","","healthicon");
		}
        elseif($serverHost == 'kakinada360.com') //ttt
		{
			 return mysqli_connect("localhost","prasanthi123","habeeb123","healthicon");
		}
		else
		{
			return mysqli_connect("localhost","healthAdmin","health@123","healthicon");
		}
    }
    
}


?>