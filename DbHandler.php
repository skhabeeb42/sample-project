<?php
/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Habeebullah Saheb Shaik
 */
class DbHandler {

    private $conn;
    
    function __construct() 
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->con = $db->connect();
        date_default_timezone_set("UTC");
    }
    
    function isValidApiKey($token)
    {
		$query = "select * from users where token = '".$token."'";
		$cmd = mysqli_query($this->con,$query);
		return mysqli_num_rows($cmd);
	}
	
    function generateApiKey() 
    {
        return md5(uniqid(rand(), true));
    }
    
    function sendOTP($mobileNumber)
	{
		try
		{
            $OTP = substr(mt_rand(),0,6);
            
            //checking whether same mobile number already exists
            $query = "select * from mobileotp where mobileNumber = '".$mobileNumber."'";
            $cmd = mysqli_query($this->con,$query);
            $count = mysqli_num_rows($cmd);
            if($count > 0)
            {
                $query = "update mobileotp set OTP = '".$OTP."' where mobileNumber = '".$mobileNumber."'";
                $cmd = mysqli_query($this->con,$query);
            }
            else
            {
                $query = "insert into mobileotp(mobileNumber,OTP) values('".$mobileNumber."','".$OTP."')";
                $cmd = mysqli_query($this->con,$query);
            }
            if($cmd)
			{
				$msg = "Your Healthicon OTP is :".$OTP;
				$msg = urlencode($msg);
				$url = 'http://158.69.130.136/smsapi/index.jsp?username=lakshmi@2016&pwd=2016@2016&msisdn='.$mobileNumber.'&msg='.$msg.'&senderid=VRDIET';
				$op =file_get_contents($url);
				return $OTP; //success
			}
			else
			{
				return -3; //update failed.
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function verifyOTP($mobileNumber,$OTP)
	{
		try
		{
			$query = "select * from mobileotp where mobileNumber = '".$mobileNumber."' and OTP = '".$OTP."'";
            $cmd = mysqli_query($this->con,$query);
            $count = mysqli_num_rows($cmd);
            if($count > 0)
            {
				$token = $this->generateApiKey();
				$query = "update users set token = '".$token."',isVerified = 1 where mobileNumber = '".$mobileNumber."'";
				$cmd = mysqli_query($this->con,$query);
				$msg = "your healthicon OTP is verified";
				$msg = urlencode($msg);
				$url = 'http://158.69.130.136/smsapi/index.jsp?username=lakshmi@2016&pwd=2016@2016&msisdn='.$mobileNumber.'&msg='.$msg.'&senderid=VRDIET';
				@file_get_contents($url);
				return $this->getUserDetailsByMobileNumber($mobileNumber);
			}
			else
			{
				return 0;
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
    
    function signUp($name,$email,$mobileNumber,$password,$latitude,$longitude,$platform,$deviceID,$referralCode)
    {
		try
		{
			$query = "select * from users where mobileNumber = '".$mobileNumber."'";
            $cmd = mysqli_query($this->con,$query);
            $count = mysqli_num_rows($cmd);
            if($count > 0)
            {
				return -1; //mobileNumber exists
			}
			else
			{
				$query = "select * from users where email = '".$email."'";
				$cmd = mysqli_query($this->con,$query);
				$count = mysqli_num_rows($cmd);
				if($count > 0)
				{
					return -2; //email exists
				}
				else
				{
					$token = $this->generateApiKey();
					$query = "insert into users(name,email,mobileNumber,password,token,latitude,longitude,platform,deviceID,referralCode,createdTime) values('".$name."','".$email."','".$mobileNumber."','".$password."','".$token."','".$latitude."','".$longitude."','".$platform."','".$deviceID."','".$referralCode."',now())";
					$cmd = mysqli_query($this->con,$query);
					if($cmd)
					{
						return $this->sendOTP($mobileNumber);
					}
					else
					{
						return -3; //error in creating user
					}
				}
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getUserDetailsByMobileNumber($mobileNumber)
	{
		try
		{
			$row = array();
			$query = "select * from users where mobileNumber = '".$mobileNumber."'";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				$row['userID'] = $row['id'];
				unset($row['id']);
				unset($row['modifiedTime']);
				unset($row['createdTime']);
				unset($row['deleteStatus']);
			}
			return $row;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getUserDetails($userID)
	{
		try
		{
			$row = array();
			$query = "select * from users where isVerified = 1 and id = ".$userID;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				$row['userID'] = $userID;
				unset($row['id']);
				unset($row['modifiedTime']);
				unset($row['createdTime']);
				unset($row['deleteStatus']);
			}
			return $row;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function updateUserToken($userID)
	{
		try
		{
			$token = $this->generateApiKey();
			$query = "update users set token = '".$token."' where id = ".$userID;
			$cmd = mysqli_query($this->con,$query);
			return $token;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function updateDeviceDetails($userID,$latitude,$longitude,$platform,$deviceID)
	{
		try
		{
			$query = "update users set latitude = '".$latitude."',longitude = '".$longitude."',platform = '".$platform."',deviceID = '".$deviceID."' where id = ".$userID;
			$cmd = mysqli_query($this->con,$query);
			return 1;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function login($mobileNumber,$password,$latitude,$longitude,$platform,$deviceID)
	{
		try
		{
			$details = $this->getUserDetailsByMobileNumber($mobileNumber);
			if($details)
			{
				if($details['isVerified'] == 1)
				{
					if($details['password'] == $password) //success
					{
						unset($details['isVerified']);
						//updating user token
						$userID = $details['userID'];
						$token = $this->updateUserToken($userID);
						$this->updateDeviceDetails($userID,$latitude,$longitude,$platform,$deviceID);
						$userDetails = $this->getUserDetails($userID);
						unset($userDetails['isVerified']);
						return $userDetails;
					}
					else
					{
						return -2; //invalid password
					}
				}
				else
				{
					return -1; //user not Verified
				}
			}
			else
			{
				return 0; //invalid mobileNumber
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function logout($userID)
	{
		try
		{
			$query = "update users set token = '',deviceID = '',platform = '' where id = ".$userID;
			$cmd = mysqli_query($this->con,$query);
			return ($cmd)?1:-3;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getDashboardImages()
    {
		try
		{
			$details = array();
			$query = "select * from dashboardImages where deleteStatus = 1 order by id desc";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$details[] = str_replace(' ','%20',$row['image']);
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getBaseURL()
	{
        try
        {
            $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            $url = ($protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            $url1 = explode('v1',$url);
            return $url1[0];
        }
        catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getBaseURL2()
	{
        try
        {
            $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            $url = ($protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            $url1 = explode('mobapp',$url);
            return $url1[0];
        }
        catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getCategories($userID)
	{
		try
		{
			$details = array();
			$query = "select id as categoryID,name,image as categoryImage from categories where deleteStatus = 1 order by id desc";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$categoryID = $row['categoryID'];
					$row['categoryImage'] = str_replace(' ','%20',$row['categoryImage']);
					$row['products'] = $this->getCategoryProducts($userID,$categoryID,0,10);
					if($row['products'])
					{
						$details[] = $row;
					}				
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getCategoryProductsQuery($categoryID)
	{
		try
		{
			return "select p.id as productID,p.name as productName,p.categoryID,p.measure,p.unit,c.name as categoryName,p.image,p.features,p.description,p.price,p.originalPrice,p.quantity,p.deleteStatus from products p,categories c where c.deleteStatus = 1 and c.id = p.categoryID and p.deleteStatus = 1 and c.id = ".$categoryID." order by p.quantity desc";
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getCategoryProductsCount($categoryID)
	{
		try
		{
			$query = $this->getCategoryProductsQuery($categoryID);
			$cmd = mysqli_query($this->con,$query);
			return mysqli_num_rows($cmd);
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getProductQuantityFromCart($userID,$productID)
	{
		try
		{
			$cartItemQuantity = "0";
			$cartID = "";
			$query = "select * from cart where userID = ".$userID." and productID = ".$productID;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				if($row)
				{
					$cartItemQuantity = $row['quantity'];
					$cartID = $row['id'];
				}
			}
			return array('cartItemQuantity'=>$cartItemQuantity,'cartID'=>$cartID);
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getCategoryProducts($userID,$categoryID,$min,$max)
	{
		try
		{
			$details = array();
			$query = $this->getCategoryProductsQuery($categoryID)." limit ".$min.",".$max;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$productID = $row['productID'];
					$cartDetails = $this->getProductQuantityFromCart($userID,$productID);
					$row['cartItemQuantity'] = $cartDetails['cartItemQuantity'];
					$row['cartID'] = $cartDetails['cartID'];
					$row['price'] = str_replace('.00','',$row['price']);
					$row['originalPrice'] = str_replace('.00','',$row['originalPrice']);
					$details[] = $row;
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getDeliveryCharge($stateID)
	{
		try
		{
			$deliveryCharge = 0;
			$query = "select deliveryCharge from states where id = ".$stateID;
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd) > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				$deliveryCharge = $row['deliveryCharge'];
			}
			return $deliveryCharge;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getItemDeliveryCharge($stateID,$unit,$measure,$quantity)
	{
		try
		{
			$unitCharge = 0;
			if($unit == 'Gms' || $unit == 'KG')
			{
				switch($measure)
				{
					default: $unitCharge = 30; break; //500 gms
					case 250: $unitCharge = 15; break; //250 gms
					case 1: $unitCharge = 60; break; //1 Kg
					case 2: $unitCharge = 120; break; //2 Kg
				}
			}
			elseif($unit == 'Litre(s)')
			{
				$deliveryCharge = $this->getDeliveryCharge($stateID);
				switch($measure)
				{
					default: $unitCharge = $deliveryCharge; break;
					case 2: $unitCharge = 2 * $deliveryCharge; break;
				}
			}
			$deliveryCharge = $quantity * $unitCharge;
			if($stateID == 2 && $stateID == 32) //concession for andhra and telangana only
			{
				if($deliveryCharge >= 100 && $deliveryCharge <= 150)
				{
					$deliveryCharge = 100;
				}
				elseif($deliveryCharge >= 150 && $deliveryCharge <= 200)
				{
					$deliveryCharge = 150;
				}
				elseif($deliveryCharge >= 200)
				{
					$deliveryCharge = 200;
				}
			}
			return $deliveryCharge;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getUserCartDetails($userID,$stateID)
	{
		try
		{
			$details = array();
			$deliveryCharge = 0;
			$query = "select c.id as cartID,c.userID,p.categoryID,c.productID,p.name as productName,c.quantity as itemQuantity,c.price,p.measure,p.unit,p.quantity,p.deleteStatus as productStatus,cat.deleteStatus as categoryStatus from cart c,products p,categories cat where cat.id = p.categoryID and c.productID = p.id and c.userID = ".$userID;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$categoryStatus = $row['categoryStatus'];
					$productStatus = $row['productStatus'];
					$productQuantity = $row['quantity'];
					$cartID = $row['cartID'];
					//checking if product and category are deleted or not
					if($categoryStatus == 0 || $productStatus == 0 || $productQuantity == 0) 
					{
						//category or product deleted
						$this->deleteCartProduct($cartID);
					}
					else
					{
						$deliveryCharge = 0;
						$productID = $row['productID'];
						$price = $row['price'];
						$measure = $row['measure'];
						$unit = $row['unit'];
						$quantity = $row['itemQuantity'];
						if($stateID != 0)
						{
							$deliveryCharge = $this->getItemDeliveryCharge($stateID,$unit,$measure,$quantity);
						}
						$row['deliveryCharge'] = (string)$deliveryCharge;
						$row['itemPrice'] = $price * $quantity;
						$row['totalPrice'] = ($price * $quantity) + $deliveryCharge;
						unset($row['categoryStatus']);
						unset($row['productStatus']);
						$details[] = $row;
					}
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getHomeDetails($userID)
	{
		try
		{
			$images = $this->getDashboardImages();
			$categories = $this->getCategories($userID);
			$cartDetails = $this->getUserCartDetails($userID,0);
			$cartCount = count($cartDetails);
			
			return array('images'=>$images,'categories'=>$categories,'cartCount'=>$cartCount);
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getProductDetails($productID)
	{
		try
		{
			$row = array();
			$query = "select p.id as productID,p.name as productName,p.categoryID,p.measure,p.unit,c.name as categoryName,p.image,p.features,p.description,p.price,p.originalPrice,p.quantity,p.deleteStatus from products p,categories c where p.quantity > 0 and c.deleteStatus = 1 and p.deleteStatus = 1 and p.id = ".$productID." and c.id = p.categoryID";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				$row['price'] = str_replace('.00','',$row['price']);
				$row['originalPrice'] = str_replace('.00','',$row['originalPrice']);
			}
			return $row;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function deleteCartProduct($cartID)
	{
		try
		{
			$query = "delete from cart where id = ".$cartID;
			$cmd = mysqli_query($this->con,$query);
			return ($cmd)?1:-3;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function updateCartProductQuantity($cartID,$status)
	{
		try
		{
			//status - 1:increase quantity,0-decrease quantity
			$query = "select * from cart where id = ".$cartID;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0) //product already added by user to cart
			{
				$row = mysqli_fetch_assoc($cmd);
				$productID = $row['productID'];
				//checking if product exists or not
				$productDetails = $this->getProductDetails($productID);
				if($productDetails)
				{
					$price = $productDetails['price'];
					if($status)
					{
						$quantity = $row['quantity'] + 1;
					}
					else
					{
						$quantity = $row['quantity'] - 1;
					}
					if($quantity == 0) //deleting cart product
					{
						$this->deleteCartProduct($cartID);
						return $cartID;
					}
					else //updating cart product
					{
						$query = "update cart set price = ".$price.",quantity = ".$quantity." where id = ".$cartID;
						$cmd = mysqli_query($this->con,$query);
						return ($cmd)?$cartID:-3;
					}
				}
				else
				{
					return -2; //product deleted
				}
			}
			else
			{
				return -1; //cartID not found
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function addToCart($userID,$productID,$quantity)
	{
		try
		{
			//checking if product already added by user to cart
			$query = "select * from cart where userID = ".$userID." and productID = ".$productID;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0) //product already added by user to cart
			{
				$row = mysqli_fetch_assoc($cmd);
				$cartID = $row['id'];
				//updating product quantity
				return (int)$this->updateCartProductQuantity($cartID,$quantity);
			}
			else //product added by user for first time
			{
				$productDetails = $this->getProductDetails($productID);
				if($productDetails)
				{
					$price = $productDetails['price'];
					$query = "insert into cart(userID,productID,price,quantity,createdTime) values(".$userID.",".$productID.",".$price.",".$quantity.",now())";
					$cmd = mysqli_query($this->con,$query);
					return ($cmd)?mysqli_insert_id($this->con):-3;
				}
				else
				{
					return -2; //product not found
				}
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function isValidCart($cartID)
	{
		try
		{
			$query = "select * from cart where id = ".$cartID;
			$cmd = mysqli_query($this->con,$query);
			return mysqli_num_rows($cmd);
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function generateOrderNumber()
	{
		try
		{
			$count = 1;
			$query = "select id from orders order by id desc limit 0,1";
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd))
			{
				$row = mysqli_fetch_assoc($cmd);
				$count = $row['id'] + 1;
			}
			$count = str_pad($count,5,"0",STR_PAD_LEFT);
			return 'HIC'.Date('y').Date('m').$count;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function deleteUserCart($userID)
	{
		try
		{
			$query = "delete from cart where userID = ".$userID;
			$cmd = mysqli_query($this->con,$query);
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function updateProductQuantity($orderID)
	{
		try
		{
			$query = "select * from orderProducts where orderID = ".$orderID;
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd) > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$productID = $row['productID'];
					$quantity = $row['quantity'];
					$productDetails = $this->getProductDetails($productID);
					$productQuantity = $productDetails['quantity'] - $quantity;
					if($productQuantity < 0)
					{
						$productQuantity = 0;
					}
					$query1 = "update products set quantity = ".$productQuantity." where id = ".$productID;
					$cmd1 = mysqli_query($this->con,$query1);
				}
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function checkout($userID,$mobileNumber,$addressID,$name,$stateID,$state,$districtID,$district,$city,$area,$street,$landmark,$phoneNumber,$pincode)
	{
		try
		{
			$totalDeliveryCharge = 0;
			//checking if cart is empty or not
			$cartProducts = $this->getUserCartDetails($userID,$stateID);
			if($cartProducts) //products are available in cart
			{
				//creating order
				$orderNo = $this->generateOrderNumber();
				$query = "insert into orders(orderNo,userID,addressID,name,stateID,state,districtID,district,city,area,street,landmark,phoneNumber,pincode,payment_status,failure_message,createdTime) values('".$orderNo."','".$userID."','".$addressID."','".$name."','".$stateID."','".$state."','".$districtID."','".$district."','".$city."','".$area."','".$street."','".$landmark."','".$phoneNumber."','".$pincode."','Payment Failed','Payment Initiated but not completed',now())";
				$cmd = mysqli_query($this->con,$query);
				if($cmd) //order saved successfully
				{
					$orderID = mysqli_insert_id($this->con);
					$orderAmount = 0;
					//saving orderProducts
					for($i=0;$i<count($cartProducts);$i++)
					{
						$productID = $cartProducts[$i]['productID'];
						$quantity = $cartProducts[$i]['itemQuantity'];
						$deliveryCharge = $cartProducts[$i]['deliveryCharge'];
						$totalDeliveryCharge += $deliveryCharge; 
						$productPrice = $cartProducts[$i]['price'];
						$totalPrice = $quantity * $productPrice;
						$orderAmount += $totalPrice;
						$query = "insert into orderProducts(userID,orderID,productID,quantity,deliveryCharge,productPrice,createdTime) values('".$userID."','".$orderID."','".$productID."','".$quantity."','".$deliveryCharge."','".$productPrice."',now())";
						$cmd = mysqli_query($this->con,$query);
						if($cmd)
						{
							//updating product quantity in products
							//$this->updateProductQuantity($productID,$quantity);	
						}
					}
					//deleting cartItems
					$this->deleteUserCart($userID);
					//updating orderAmount in orders table
					$query = "update orders set orderAmount = ".$orderAmount.",deliveryCharge = '".$totalDeliveryCharge."' where id = ".$orderID;
					$cmd = mysqli_query($this->con,$query);
					//sending SMS
					//$this->sendOrderSMS($orderID);
					return array("orderID"=>$orderID,"orderAmount"=>$orderAmount,"deliveryCharge"=>$totalDeliveryCharge);
				}
				else
				{
					return -3; //error in saving order
				}
			}
			else
			{
				return 0; //no products in cart
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function sendOrderSMS($orderID)
	{
		try
		{
			$query = "select orderNo,phoneNumber from orders where id = ".$orderID;
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd))
			{
				$row = mysqli_fetch_assoc($cmd);
				$orderNo = $row['orderNo'];
				$mobileNumber = $row['phoneNumber'];
				if($mobileNumber)
				{
					//sending SMS
					$msg = "Your Order ".$orderNo." has been placed sucessfully. On confirmation, it will be delivered within 8-10 business days.";
					$msg = urlencode($msg);
					$url = 'http://158.69.130.136/smsapi/index.jsp?username=lakshmi@2016&pwd=2016@2016&msisdn='.$mobileNumber.'&msg='.$msg.'&senderid=VRDIET';
					$op =file_get_contents($url);
				}
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getOrdersQuery($userID)
	{
		try
		{
			return "select o.id as orderID,o.orderNo,o.orderAmount,o.deliveryCharge,o.name,o.stateID,o.state,o.districtID,o.district,o.city,o.area,o.street,o.landmark,o.phoneNumber,o.pincode,o.orderStatus,o.tracking_id as paymentID,o.payment_status as paymentStatus,o.createdTime as orderDate,u.id as userID,u.name,u.mobileNumber from orders o,users u where o.userID = ".$userID." and o.userID = u.id order by o.id desc";
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getOrdersCount($userID)
	{
		try
		{
			$details = array();
			$query = $this->getOrdersQuery($userID);
			$cmd = mysqli_query($this->con,$query);
			return mysqli_num_rows($cmd);
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getOrders($userID,$min,$max)
	{
		try
		{
			$details = array();
			$query = $this->getOrdersQuery($userID).' limit '.$min.','.$max;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					switch($row['orderStatus'])
					{
						default: $row['orderStatusTitle'] = 'In Progress';break;
						case 0: $row['orderStatusTitle'] = 'Rejected';break;
						case 1: $row['orderStatusTitle'] = 'Accepted';break;
						case 2: $row['orderStatusTitle'] = 'Delivered';break;
						case -2: $row['orderStatusTitle'] = 'Cancelled';break;
					}
					$details[] = $row;
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getOrderProducts($orderID)
	{
		try
		{
			$details = array();
			$query = 'select o.productID,p.name,p.image,o.quantity as itemQuantity,o.productPrice,o.deliveryCharge,p.measure,p.unit,p.features,p.description from orderProducts o,products p where o.orderID = '.$orderID.' and o.productID = p.id';
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$quantity = $row['itemQuantity'];
					$productPrice = $row['productPrice'];
					$row['totalPrice'] = $quantity * $productPrice;
					$row['productPrice'] = str_replace('.00','',$row['productPrice']);
					$details[] = $row;
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getOrderDetails($orderID)
	{
		try
		{
			$row = array();
			$query = 'select o.id as orderID,o.orderNo,o.orderAmount,o.deliveryCharge,o.name,o.stateID,o.state,o.districtID,o.district,o.city,o.area,o.street,o.landmark,o.phoneNumber,o.pincode,o.orderStatus,o.tracking_id as paymentID,o.payment_status as paymentStatus,o.createdTime as orderDate,u.id as userID,u.name,u.mobileNumber from orders o,users u where o.id = '.$orderID.' and o.userID = u.id';
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				switch($row['orderStatus'])
				{
					default: $row['orderStatusTitle'] = 'In Progress';break;
					case 0: $row['orderStatusTitle'] = 'Rejected';break;
					case 1: $row['orderStatusTitle'] = 'Accepted';break;
					case 2: $row['orderStatusTitle'] = 'Delivered';break;
					case -2: $row['orderStatusTitle'] = 'Cancelled';break;
				}
				$row['orderProducts'] = $this->getOrderProducts($orderID);
			}
			return $row;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function cancelOrder($orderID)
	{
		try
		{
			$query = "update orders set orderStatus = -2 where id = ".$orderID;
			$cmd = mysqli_query($this->con,$query);
			//sending SMS
			$orderDetails = $this->getOrderDetails($orderID);
			if($orderDetails)
			{
				$orderNo = $orderDetails['orderNo'];
				$mobileNumber = $orderDetails['mobileNumber'];
				$msg = "Your Order ".$orderNo." has been cancelled sucessfully.";
				$msg = urlencode($msg);
				$url = 'http://158.69.130.136/smsapi/index.jsp?username=lakshmi@2016&pwd=2016@2016&msisdn='.$mobileNumber.'&msg='.$msg.'&senderid=VRDIET';
				$op =file_get_contents($url);
			}
			return ($cmd)?1:-3;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getPincodeDetails($pincode)
	{
		try
		{
			$row = array();
			$query = "select * from pincodes where pincode = '".$pincode."' and deleteStatus = 1";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
			}
			else
			{
				//sending SMS to Admin
				$msg = "Requesting service for the area with pincode ".$pincode;
				$msg = urlencode($msg);
				$url = 'http://158.69.130.136/smsapi/index.jsp?username=lakshmi@2016&pwd=2016@2016&msisdn=8999739999&msg='.$msg.'&senderid=VRDIET';
				$op =file_get_contents($url);
			}
			return $row;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getAppStatus()
	{
		try
		{
			$row = array();
			$query = "select * from appExpiry where Date(expiryDate) >= Date(now())";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
			}
			return $row;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function addAddress($userID,$name,$stateID,$state,$districtID,$district,$city,$area,$street,$landmark,$phoneNumber,$pincode)
	{
		try
		{
			$query = "insert into addresses(userID,name,stateID,state,districtID,district,city,area,street,landmark,phoneNumber,pincode,createdTime) values('".$userID."','".$name."','".$stateID."','".$state."','".$districtID."','".$district."','".$city."','".$area."','".$street."','".$landmark."','".$phoneNumber."','".$pincode."',now())";
			$cmd = mysqli_query($this->con,$query);
			return ($cmd)?mysqli_insert_id($this->con):-3;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function editAddress($addressID,$userID,$name,$stateID,$state,$districtID,$district,$city,$area,$street,$landmark,$phoneNumber,$pincode)
	{
		try
		{
			$query = "update addresses set userID = '".$userID."',name = '".$name."',stateID = '".$stateID."',state = '".$state."',districtID = '".$districtID."',district = '".$district."',city = '".$city."',area = '".$area."',street = '".$street."',landmark = '".$landmark."',phoneNumber = '".$phoneNumber."',pincode = '".$pincode."' where id = ".$addressID;
			$cmd = mysqli_query($this->con,$query);
			return ($cmd)?1:-3;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getAddress($addressID)
	{
		try
		{
			$row = array();
			$query = "select * from addresses where id = ".$addressID;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
			}
			return $row;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getUserAddresses($userID)
	{
		try
		{
			$details = array();
			$query = "select * from addresses where deleteStatus = 1 and userID = ".$userID;
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$row['addressID'] = $row['id'];
					unset($row['id']);
					unset($row['createdTime']);
					unset($row['modifiedTime']);
					unset($row['deleteStatus']);
					$details[] = $row;
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function deleteAddress($addressID)
	{
		try
		{
			$query = "update addresses set deleteStatus = 0 where id = ".$addressID;
			$cmd = mysqli_query($this->con,$query);
			return ($cmd)?1:-3;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getStores()
	{
		try
		{
			$details = array();
			$query = "select * from stores where deleteStatus = 1 order by districtID desc";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$details[] = $row;
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getStateName($stateID)
	{
		try
		{
			$query = "select * from states where id = ".$stateID;
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd) > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				return $row['name'];
			}
			else
			{
				return "";
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getDistrictName($districtID)
	{
		try
		{
			$query = "select * from districts where id = ".$districtID;
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd) > 0)
			{
				$row = mysqli_fetch_assoc($cmd);
				return $row['name'];
			}
			else
			{
				return "";
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getAllDistricts()
	{
		try
		{
			$details = array();
			$query = "select * from districts order by name asc";
			$cmd = mysqli_query($this->con,$query);
			$count = mysqli_num_rows($cmd);
			if($count > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$details[] = $row['name'];
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getStates()
	{
		try
		{
			$query = "select * from states";
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd) > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$row['stateID'] = $row['id'];
					unset($row['id']);
					$states[] = $row;
				}
				return $states;
			}
			else
			{
				return "";
			}
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getDistricts($stateID)
	{
		try
		{
			$districts = array();
			$query = "select * from districts where stateID = ".$stateID;
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd) > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$row['districtID'] = $row['id'];
					unset($row['id']);
					$districts[] = $row;
				}
			}
			return $districts;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function getStoresByState($stateID,$districtID)
	{
		try
		{
			$details = array();
			$query = "select id,name from stores where stateID = ".$stateID." and districtID = ".$districtID;
			$cmd = mysqli_query($this->con,$query);
			if(mysqli_num_rows($cmd) > 0)
			{
				while($row = mysqli_fetch_assoc($cmd))
				{
					$details[] = $row;
				}
			}
			return $details;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
	
	function updatePaymentStatus($orderID,$tracking_id,$bank_ref_no,$payment_status,$failure_message,$payment_mode,$card_name,$status_code,$status_message)
	{
		try
		{
			$orderStatus = 0;
			$smsFlag = 0;
			if(strtolower($payment_status) == 'success')
			{
				$orderStatus = -1;
				$smsFlag = 1;
			}
			$query = "update orders set orderStatus = '".$orderStatus."',tracking_id = '".$tracking_id."',bank_ref_no = '".$bank_ref_no."',payment_status = '".$payment_status."',failure_message = '".$failure_message."',payment_mode = '".$payment_mode."',card_name = '".$card_name."',status_code = '".$status_code."',status_message = '".$status_message."' where id = ".$orderID;
			$cmd = mysqli_query($this->con,$query);
			if($smsFlag == 1)
			{
				$this->updateProductQuantity($orderID);
				$this->sendOrderSMS($orderID);
			}
			return $orderID;
		}
		catch(Exception $e)
		{
			echo "<pre>";print_r($e);
			exit;
		}
	}
}
?>
